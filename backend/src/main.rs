use std::{
    fs::read,
    io::Write,
    process::{Command, Stdio},
};

fn main() {
    let output_stem = "music_sheet";
    let output_ = format!("/out/{}", output_stem);
    let destination = output_.as_str();
    let lilypond_args = ["lilypond", "-dno-point-and-click", "-o", destination, "-"];
    let docker_args = ["run", "--rm", "-iv", "/tmp/lilypond:/out:z", "lilypond"];

    let mut docker = Command::new("docker")
        .args([docker_args, lilypond_args].concat())
        .stdin(Stdio::piped())
        .spawn()
        .expect("Command “docker run” failed.");

    let docker_stdin = docker.stdin.as_mut().unwrap();

    let content = read("test.ly").unwrap();
    let _ = docker_stdin.write_all(&content);

    let output = docker.wait_with_output().unwrap();

    println!("output = {:?}", output)
}
